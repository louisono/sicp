;; Newton's method for cube roots is based on the fact that if y is an approximation
;; to the cube root of x, then a better approximation is given by the value:
;; ((x / y²) + 2y) / 3
;; use this formula to implement a cube root procedure analogous to the square root
;; procedure.

(define (square x)
  (* x x))

(define (improve-cube guess x)
  (/ (+ (/ x (square guess)) (* 2 guess)) 3))

(define (good-enough? old-guess new-guess)
  (<= (/ (abs (- old-guess new-guess)) old-guess)
     0.001))

(define (cbrt-iter guess x)
  (define new-guess (improve-cube guess x))
  (if (good-enough? guess new-guess)
    guess
    (cbrt-iter new-guess x)))

(define (cbrt x)
  (cbrt-iter 1.0 x))
