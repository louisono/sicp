# SICP - exercises

Here are my attempts at solving exercises from the textbook [Structure and Interpretation of Computer Programs](https://en.wikipedia.org/wiki/Structure_and_Interpretation_of_Computer_Programs)

### Noteworthy online resources:
- [full second edition freely available online](https://web.mit.edu/6.001/6.037/sicp.pdf)
- Solutions for these are also available on this [great, scheme-related website](http://community.schemewiki.org/?sicp-solutions)
- [video lectures delivered by Sussman and Abelson](https://www.youtube.com/playlist?list=PLE18841CABEA24090)
