# Information about the picture language

## Install
Here is a [SO discussion](https://stackoverflow.com/questions/39886804/how-to-run-picture-language-examples-of-sicp-scheme-in-repl) about how to get the picture language up and running, as its procedures are not primitive to Scheme. The easiest solution that I could find was to use [Racket](https://racket-lang.org/) instead of Scheme and so install instructions are also available on [their website](https://docs.racket-lang.org/sicp-manual/Installation.html). The switch to Racket for this part doesn't prevent the programmer from using all procedures and syntax discussed so far in the book as long as racket files start with:   
```racket
#lang sicp
;; your Scheme code here...
```

## Documentation
Documentation for all picture language procedures is available on the [Racket website](https://docs.racket-lang.org/sicp-manual/SICP_Picture_Language.html).  
Painters `wave` and `rogers` are mentioned in the book but they are not defined in the `sicp-pict` module used here. See instead [doc section "Simple Built-In Painters"](https://docs.racket-lang.org/sicp-manual/SICP_Picture_Language.html#%28part._.Simple_.Built-.In_.Painters%29) to have a list of primitive painters.
