(define nil '())

(define (make-tree entry left right) (list entry left right))
(define (make-leaf entry) (make-tree entry nil nil))
(define entry car)
(define left cadr)
(define right caddr)

;; ex2.63: here are two procedures to turn a tree into a list.
;; a. Do the two procedures produce the same result for every tree? If not, how do
;;    the results differ? What lists do the two procedures produce for the trees
;;    in Figure 2.16?
;; b. Do the two procedures have the same order of growth in the number of steps
;;    required to convert a balanced tree with n elements to a list? If not, which
;;    one grows more slowly?

(define (tree->list1 tree)
  (if (null? tree)
	nil
	(append (tree->list1 (left tree))
			(cons (entry tree)
				  (tree->list1 (right tree))))))
(define (tree->list2 tree)
  (define (copy-to-list t result)
	(if (null? t)
	  result
	  (copy-to-list (left t)
					(cons (entry t)
						  (copy-to-list (right t)
										result)))))
  (copy-to-list tree nil))
						 
;; trees represented in figure 2.16
(define a (make-tree 7
					 (make-tree 3
								(make-leaf 1)
								(make-leaf 5))
					 (make-tree 9
								nil
								(make-leaf 11))))
(define b (make-tree 3
					 (make-leaf 1)
					 (make-tree 7
								(make-leaf 5)
								(make-tree 9
										   nil
										   (make-leaf 11)))))
(define c (make-tree 5
					 (make-tree 3
								(make-leaf 1)
								nil)
					 (make-tree 9
								(make-leaf 7)
								(make-leaf 11))))

;; a. both procedures essentially make the same operation:
;;    - for a tree t, stick together (in that order) the list of the left subtree
;;      of t, the entry of t, and the list of the right subtree of t.
;; here is the result of calling these procedures with trees represented in fig 2.16
;; (tree->list1 a)
;; -> '(1 3 5 7 9 11)
;; (tree->list1 b)
;; -> '(1 3 5 7 9 11)
;; (tree->list1 c)
;; -> '(1 3 5 7 9 11)
;; (tree->list2 a)
;; -> '(1 3 5 7 9 11)
;; (tree->list2 b)
;; -> '(1 3 5 7 9 11)
;; (tree->list2 c)
;; -> '(1 3 5 7 9 11)

;; b. 
;; assumption: the tree is balanced so left and right branches are both of 
;; size n-1/2 (which is simplified by n/2)
;; for tree->list1: 
;; (append l1 l2) has a time complexity of θ(len(l1))
;; T(n) = θ(n/2) + 2.T(n/2) 
;;      = θ(n/2) + 2.θ(n/4) + 4.T(n/4)
;;      = ... = m.θ(n)
;; where m is such that n/2^m = 1
;; n = 2^m <=> n = e^(m.ln(2)) <=> ln(n) = m.ln(2) <=> m = ln(n)/ln(2) = log2(n)
;; T(n) = θ(n).log2(n) = θ(n.ln(n))

;; for tree->list2:
;; (cons x l) has a time complexity of θ(1)
;; T(n) = θ(1) + 2.T(n/2)
;;      = θ(1) + 2.θ(1) + 4.T(n/4)
;;      = (i = 0 -> m) Σ(2^i) (sum of all 2^i terms for i from 0 to m)
;; where m is such that n/2^m = 1 => m = log2(n) (see above)
;; T(n) = (i = 0 -> log2(n)) Σ(2^i) = 2^(log2(n) + 1) - 1 = (n + 2) - 1 = θ(n)
;;      = e^ln(2^(log2(n)+1)) = e^((log2(n)+1).ln(2)) = e^(ln(2).log2(n)+ln(2))
;;      = e^(ln(n)+ln(2)) = e^ln(2*n) = 2*n = θ(n)

;; tree->list2 has better time performance because it is relying on a θ(1) procedure
;; (cons)
