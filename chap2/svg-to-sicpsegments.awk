#! /usr/bin/env -S awk --field-separator "=" -f

BEGIN { 
	make_list = "(list "
	make_segment = "(make-segment "
	make_vector = "(make-vect "
	close_func_call = ")"
   	print make_list 
}


/^\s+d/ {
	path = $2

	# get rid of surrounding " characters
	gsub("\"", "", path)
	# split path into distinct points
	split(path, points, " ")
	first_point = 1
	for (i in points) {
		point = points[i]
		# ignore M character: SVG "goto" instruction
		if (point != "M") {
			# get separate x and y coords
			split(point, xy, ",")
			# print point
			# only keep up to 1/100 precision
			x = substr(xy[1], 1, 4)
			# svg coords start in upper left corner 
			# sicp picture language coords start in bottom left corner
			y = substr(xy[2], 1, 4)
			y = 1 - y
			point_vector = make_vector x " " y
			if (first_point) {
				print make_segment point_vector close_func_call
				first_point = !first_point
			} else if (i == length(points)) {
				print point_vector close_func_call close_func_call
			} else {
				print point_vector close_func_call close_func_call
				print make_segment point_vector close_func_call
			}
		}
	}
}

END { print close_func_call }
