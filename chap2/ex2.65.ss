;; ex2.65: use the results of previous exercises to give a θ(n) implementation
;; of union-set and intersection-set for sets implemented as balanced binary
;; trees.

(define nil '())

(define (make-tree entry left right) (list entry left right))
(define entry car)
(define left cadr)
(define right caddr)

(define (list->tree elements)
  (define (partial-tree elts n)
	(if (= n 0)
	  (cons '() elts)
	  (let ((left-size (quotient (- n 1) 2)))
		(let ((left-result (partial-tree elts left-size)))
		  (let ((left-tree (car left-result))
				(non-left-elts (cdr left-result))
				(right-size (- n (+ left-size 1))))
			(let ((this-entry (car non-left-elts))
				  (right-result (partial-tree (cdr non-left-elts)
											  right-size)))
			  (let ((right-tree (car right-result))
					(remaining-elts (cdr right-result)))
				(cons (make-tree this-entry
								 left-tree
								 right-tree)
					  remaining-elts))))))))
  (car (partial-tree elements (length elements))))
(define (tree->list tree)
  (define (copy-to-list t result)
	(if (null? t)
	  result
	  (copy-to-list (left t)
					(cons (entry t)
						  (copy-to-list (right t)
										result)))))
  (copy-to-list tree nil))
(define (merge-no-duplicates list1 list2)
  (define (merge m l1 l2)
	(cond ((and (null? l1) (null? l2)) (reverse m))
		  ((null? l2) (merge (cons (car l1) m) (cdr l1) l2))
		  ((null? l1) (merge (cons (car l2) m) (cdr l2) l1))
		  (else (let ((x1 (car l1))
					  (x2 (car l2)))
				  (cond ((< x1 x2) (merge (cons x1 m) (cdr l1) l2))
						((< x2 x1) (merge (cons x2 m) l1 (cdr l2)))
						(else (merge (cons x1 m) (cdr l1) (cdr l2))))))))
  (merge nil list1 list2))
(define (intersect list1 list2)
  (if (or (null? list1) (null? list2))
	'()
	(let ((x1 (car list1)) (x2 (car list2)))
	  (cond ((= x1 x2)
			 (cons x1 (intersect (cdr list1)
										(cdr list2))))
			((< x1 x2)
			 (intersect (cdr list1) list2))
			((< x2 x1)
			 (intersect list1 (cdr list2)))))))

(define (union-set s1 s2)
  (let ((list-of-s1 (tree->list s1))
		(list-of-s2 (tree->list s2)))
	(list->tree (merge-no-duplicates list-of-s1 list-of-s2))))

;; union-set complexity
;; T(n, m) = T1(n) + T1(m) + T2(n+m) + T3(n, m)
;; where:
;;     - n & m are the length of sets s1 and s2
;;     - T1(x) is the complexity of procedure tree->list on a set of size x (= θ(x))
;;     - T2(x) is the complexity of procedure list->tree on a list of size x (= θ(x))
;;     - T3(x, y) is the complexity of procedure merge-no-duplicates on two lists of
;;       size x and y (= θ(x+y))
;; T(n, m) = θ(n) + θ(m) + θ(n+m) + θ(n+m) = θ(n+m)

(define (intersection-set s1 s2)
  (let ((list-of-s1 (tree->list s1))
		(list-of-s2 (tree->list s2)))
	(list->tree (intersect list-of-s1 list-of-s2))))

;; intersection-set complexity
;; T(n, m) = T1(n) + T1(m) + T2(n+m) + T3(n, m)
;; where:
;;     - n & m are the length of sets s1 and s2
;;     - T1(x) is the complexity of procedure tree->list on a set of size x (= θ(x))
;;     - T2(x) is the complexity of procedure list->tree on a list of size x (= θ(x))
;;     - T3(x, y) is the complexity of procedure intersect on two lists of size 
;;       x and y (= θ(x+y))
;; T(n, m) = θ(n) + θ(m) + θ(n+m) + θ(n+m) = θ(n+m)
