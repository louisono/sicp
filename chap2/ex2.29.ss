;; A binary mobile consists of two branches, a left branch and a right branch. Each
;; branch is a rod of a certain length, from which hangs either a weight or an-
;; other binary mobile. We can represent a binary mobile us- ing compound data by
;; constructing it from two branches (for example, using list ):

(define (make-mobile left right)
  (list left right))

;; A branch is constructed from a length (which must be a number) together with
;; a structure, which may be either a number (representing a simple weight) or
;; another mobile:

(define (make-branch length structure)
  (list length structure))

;; a. Write the corresponding selectors left-branch and right-branch, which return
;; the branches of a mobile, and branch-length and branch-structure, which return
;; the components of a branch.

(define left-branch car)
(define right-branch cadr)
(define branch-length car)
(define branch-structure cadr)

;; b. Using your selectors, define a procedure total-weight that returns the total
;; weight of a mobile.

(define (struct-weight struct)
  (if (not (pair? struct))
	struct
	(total-weight struct)))
(define (total-weight mobile)
  (let ((left-struct (branch-structure (left-branch mobile)))
		(right-struct (branch-structure (right-branch mobile))))
	(+ (struct-weight left-struct)
	   (struct-weight right-struct))))

(define m1 (make-mobile (make-branch 2 5)
						(make-branch 5
									 (make-mobile (make-branch 3 2)
												  (make-branch 6 1)))))
(define m2 (make-mobile (make-branch 2 5)
						(make-branch 5
									 (make-mobile (make-branch 3 4/3)
												  (make-branch 6 2/3)))))

;; (display (number->string (total-weight m1)))
;; (display (number->string (total-weight m2)))
;; output:
;; 8
;; 7

;; c. A mobile is said to be balanced if the torque applied by its top-left branch
;; is equal to that applied by its top-right branch (that is, if the length of the
;; left rod multiplied by the weight hanging from that rod is equal to the corresponding
;; product for the right side) and if each of the submobiles hanging off its branches
;; is balanced. Design a predicate that tests whether a binary mobile is balanced.

(define (torque branch)
  (* (branch-length branch) (struct-weight (branch-structure branch))))
(define (balanced-struct? struct)
  (if (not (pair? struct))
	#t
	(balanced-mobile? struct)))
(define (balanced-mobile? mobile)
  (and (= (torque (left-branch mobile)) 
		  (torque (right-branch mobile)))
	   (and (balanced-struct? (branch-structure (left-branch mobile)))
			(balanced-struct? (branch-structure (right-branch mobile))))))

;; (display (balanced-mobile? m1))
;; (display (balanced-mobile? m2))
;; output:
;; #f
;; #t

;; d. Suppose we change the representation of mobiles so that the constructors are

(define (make-mobile2 left right) (cons left right))
(define (make-branch2 length structure)
  (cons length structure))

;; How much do you need to change your programs to convert to the new representation?

;; Just the selectors have to be updated to address that change, since other 
;; procedures leverage those selectors to operate on mobiles and/or branches.

(define left-branch2 car)
(define right-branch2 cdr)
(define branch-length2 car)
(define branch-structure2 cdr)

;; change the definition of selectors:
;; (define left-branch left-branch2)
;; (define right-branch right-branch2)
;; (define branch-length branch-length2)
;; (define branch-structure branch-structure2)
